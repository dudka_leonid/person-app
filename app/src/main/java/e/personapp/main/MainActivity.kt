package e.personapp.main

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import e.personapp.R
import e.personapp.auth.AuthActivity
import e.personapp.auth.AuthService
import e.personapp.castom_timetable.SampleFragmentPagerAdapter
import e.personapp.main.fragments.FeedbackFragment
import e.personapp.main.fragments.OtherActivitiesFragment
import e.personapp.main.fragments.TemporaryTimetable
import e.personapp.user.DataBaseUser
import e.personapp.user.User
import e.personapp.user.edit_user_info.EditUserDialog
import e.personapp.user.edit_user_info.IEditUserInfoResult

class MainActivity : AppCompatActivity(), IEditUserInfoResult {

    private lateinit var nicknameUser: TextView
    private lateinit var userDescription: TextView
    private lateinit var authService: AuthService
    private lateinit var dataBaseUser: DataBaseUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        authService = AuthService()
        dataBaseUser = DataBaseUser()

        if (!isWrite()) {
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
        }

        nicknameUser = findViewById(R.id.user_nickname_user_info_fragment)
        userDescription = findViewById(R.id.user_description_user_info_fragment)
        val vp = findViewById<ViewPager>(R.id.viewpager)
        val tabs = findViewById<TabLayout>(R.id.sliding_tabs)
        val adapterSFPA= SampleFragmentPagerAdapter(supportFragmentManager)
        val singOutButton = findViewById<Button>(R.id.sing_out_button_user_info_fragment)
        val editUserInfoButton = findViewById<Button>(R.id.
                                edit_user_button_user_info_fragment)

        dataBaseUser.getUser(authService.getUID())
            .addOnSuccessListener {
                val user = it.toObject(User::class.javaObjectType)
                updateUserInfo(user!!)
            }

        singOutButton.setOnClickListener {
            authService.signOut()
            val intent = Intent(this, AuthActivity::class.java)
            startActivity(intent)
            finish()
        }

        editUserInfoButton.setOnClickListener {
            dataBaseUser.getUser(authService.getUID())
                .addOnSuccessListener {
                    val user = it.toObject(User::class.javaObjectType)
                    val editUserDialog = EditUserDialog(this, user!!)
                    editUserDialog.show(supportFragmentManager, editUserDialog.tag)
                }
        }

        adapterSFPA.addFragment(OtherActivitiesFragment(), getString(R.string.other))
        adapterSFPA.addFragment(TemporaryTimetable(), getString(R.string.timetable))
        adapterSFPA.addFragment(FeedbackFragment(), getString(R.string.feedback))

        vp.adapter = adapterSFPA
        tabs.setupWithViewPager(vp)
        vp.currentItem = 1
    }

    private fun updateUserInfo(user: User) {
        nicknameUser.text = user.nickname
        userDescription.text = user.description
    }

    override fun onChange(user: User) {
        dataBaseUser.editNicknameUser(authService.getUID(), user.nickname)
        dataBaseUser.editDescriptionUser(authService.getUID(), user.description)
        updateUserInfo(user)
    }

    private fun isWrite(): Boolean {
        return (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)
    }
}
