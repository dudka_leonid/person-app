package e.personapp.main.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.google.android.material.card.MaterialCardView
import e.personapp.R
import e.personapp.castom_test.model.Tests
import e.personapp.study_ino.StudyINO

class OtherActivitiesFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.other_activity_card_fragment,
            container, false)

        val testsCard = view.findViewById<MaterialCardView>(R.id.run_tests_card)
        val inoCard = view.findViewById<MaterialCardView>(R.id.run_ino_card)

        testsCard.visibility = View.VISIBLE

        val testsButton: Button = view.findViewById(R.id.tests_run_button)
        testsButton.setOnClickListener {
            val connect = false
            val intent: Intent = Intent(view.context, Tests::class.java)
            intent.putExtra("connect", connect)
            startActivity(intent)
        }

        val studyINOButton: Button = view.findViewById(R.id.study_ino_run_button)
        studyINOButton.setOnClickListener {
            val intent: Intent = Intent(view.context, StudyINO::class.java)
            startActivity(intent)
        }

        return view
    }
}