package e.personapp.main.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import e.personapp.R
import e.personapp.updateApp.UpdateAppService

class FeedbackFragment: Fragment() {

    private lateinit var updateAuthService: UpdateAppService

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.feedback_fragment, container, false)
        updateAuthService = UpdateAppService(activity!!)
        var serverVersion: String
        updateAuthService.getServerVersion()
            .addOnSuccessListener {
                serverVersion = it.data!!["current_version"].toString()
                method(serverVersion != updateAuthService.getVersionName())
            }
        return view
    }

    private fun method(result: Boolean) {
        if (result) {
            try {
                updateAuthService.updateDownload()
            } catch (e: Exception) {
                Log.e("FeedbackFragment_Test", "Exception: " + e.message)
            }
        }
    }
}