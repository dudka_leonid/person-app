package e.personapp.updateApp

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Environment
import android.util.Log
import androidx.core.content.FileProvider
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import e.personapp.BuildConfig
import java.io.File




class UpdateAppService(val context: Context) {

    private val LOG_TAG: String = "FeedbackFragment_Test"

    fun getVersionName(): String {
        return BuildConfig.VERSION_NAME
    }

    fun getServerVersion(): Task<DocumentSnapshot> {
        val myDB: FirebaseFirestore = FirebaseFirestore.getInstance()
       return myDB.collection("config_app").document("versions").get()
    }

    fun updateDownload() {
        test()
    }

    private fun firstTest() {
        install()

//        getServerVersion().addOnSuccessListener {
//            val serverVersion = it.data!!["current_version"].toString()
//            val fileName = "PersonApp_$serverVersion.apk"
//            val db = FirebaseStorage.getInstance().reference
//            val riversRef = db.child("apks/$fileName")
//            riversRef.downloadUrl.addOnSuccessListener { uri ->
//                val downloadManager: DownloadManager = context
//                    .getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
//                val request = DownloadManager.Request(uri)
//                request.setNotificationVisibility(
//                    DownloadManager.Request
//                        .VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
//                request.setDestinationInExternalFilesDir(context, dir, fileName)
//                val downloadId = downloadManager.enqueue(request)
//
//                val onComplete = object : BroadcastReceiver() {
//                    override fun onReceive(ctxt: Context, intent: Intent) {
//                        val install = Intent(Intent.ACTION_VIEW)
//                        install.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//                        install.setDataAndType(
//                            Uri.parse("file://$dir$fileName"),
//                            downloadManager.getMimeTypeForDownloadedFile(downloadId)
//                        )
//                        context.startActivity(install)
//                        context.unregisterReceiver(this)
//                    }
//                }
//                context.registerReceiver(onComplete,
//                    IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
//                )
//            }
//        }
    }
    private fun download() {
        val dir = Environment.DIRECTORY_DOWNLOADS
        Log.e(LOG_TAG, "Dir: $dir")
        getServerVersion().addOnSuccessListener {
            val serverVersion = it.data!!["current_version"].toString()
            Log.e(LOG_TAG, "serverVersion: $serverVersion")
        }
    }
    private fun install() {
        val intent = Intent(Intent.ACTION_VIEW)
        val uri = Uri
            .fromFile(File(Environment.DIRECTORY_DOWNLOADS + "/PersonApp_1.0.04.apk"))
        Log.e(LOG_TAG, uri.path)
        intent.setDataAndType(uri,"application/vnd.android.package-archive"
        )
        Log.e(LOG_TAG, "1!")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        Log.e(LOG_TAG, "2!")
        context.startActivity(intent)
        Log.e(LOG_TAG, "3!")
    }

    private fun test() {
        var destination =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/"
        getServerVersion().addOnSuccessListener {
            val serverVersion = it.data!!["current_version"].toString()
            val fileName = "PersonApp_$serverVersion.apk"
            destination += fileName
            val uri = Uri.parse("file://$destination")
            Log.e(LOG_TAG, "uri: $uri")

            //Delete update file if exists
            val file = File(destination)
            if (file.exists()){
                file.delete()
                Log.e(LOG_TAG, "file delete!")
            }

            val db = FirebaseStorage.getInstance().reference
            val riversRef = db.child("apks/$fileName")
            riversRef.downloadUrl.addOnSuccessListener { downloadUri ->
                Log.e(LOG_TAG, "downloadUri: $downloadUri")

                val downloadManager: DownloadManager = context
                    .getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                val request = DownloadManager.Request(downloadUri)
                request.setNotificationVisibility(
                    DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                request.setDestinationUri(uri)

                val downloadId = downloadManager.enqueue(request)

                val onComplete = object : BroadcastReceiver() {
                    override fun onReceive(ctxt: Context, intent: Intent) {
                        val i = Intent(
                            Intent.ACTION_VIEW,
                            FileProvider.getUriForFile(ctxt, BuildConfig.APPLICATION_ID + ".provider", File(destination))
                        )
                        i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                        ctxt.startActivity(i)

                    }
                }

                context.registerReceiver(onComplete,
                    IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
                )
            }
        }

    }
}