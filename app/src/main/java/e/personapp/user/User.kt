package e.personapp.user

import java.io.Serializable

class User(): Serializable{

    var uid: String
    private set
    var  nickname: String
    var description: String

    init {
        this.uid = ""
        this.nickname = ""
        this.description = ""
    }

    constructor(uid: String,
                nickname: String = "default_nickname",
                description: String = "default_description"): this() {
        this.uid = uid
        this.nickname = nickname
        this.description = description
    }

    override fun toString(): String {
        return "User(uid='$uid', nickname='$nickname', description='$description')"
    }
}