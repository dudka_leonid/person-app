package e.personapp.user.edit_user_info

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import e.personapp.R
import e.personapp.user.User

class EditUserDialog(context: Context, user: User): DialogFragment() {

    private var mCallback: IEditUserInfoResult = context as IEditUserInfoResult
    private var user: User = user

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.edit_user_info_dialog_fragment, null)

        val saveButton = view.findViewById<Button>(R.id.
                      save_button_edit_user_info_dialog_fragment)
        val nicknameEditText = view.findViewById<EditText>(R.id.
                            user_nickname_edit_user_info_dialog_fragment)
        val descriptionEditText = view.findViewById<EditText>(R.id.
                            user_description_edit_user_info_dialog_fragment)

        nicknameEditText.setText(user.nickname)
        descriptionEditText.setText(user.description)

        saveButton.setOnClickListener {
            saveUserInfo(nicknameEditText.text.toString(), descriptionEditText.text.toString())
        }
        return view
    }

    private fun saveUserInfo(nickname: String, descriprion: String) {
        user.nickname = nickname
        user.description = descriprion
        mCallback.onChange(user)
        dismiss()
    }
}