package e.personapp.user.edit_user_info

import e.personapp.user.User

interface IEditUserInfoResult {
    fun onChange(user: User)
}