package e.personapp.user

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore


/**
 * Используй этот класс для добавления пользователей
 * val db = DataBase()
 * и используй 2 метода:
 * addUser() - для добавления пользователя
 * readUser() - для предоставления ссылки на пользователя,
 * для дальнейшей работы:
 *      db.readUser(userNickname)  //userNickname в String формате
 *      .get().addOnSuccessListener {
 *      textView.text = it.data.toString()
}
 */
class DataBaseUser {

    private val DEFAULT_PATH: String = "users"
    private var myDB: FirebaseFirestore = FirebaseFirestore.getInstance()
    var connectUsers: CollectionReference = myDB.collection("test_users")

//    fun addUser(userNickname: String, userName: String, userAge: String){
//        connectUsers.document(userNickname)
//                .collection("person_data").document("data").set(mapOf(
//                "userName" to userName,
//                "userAge" to userAge
//            ))
//    }
//
//    fun readUser(userNickname: String): DocumentReference {
//        return  connectUsers
//                    .document(userNickname)
//                    .collection("person_data")
//                    .document("data")
//    }
//
//    fun addStoreUser(userNickname: String, titleItem: String, money: Int) {
//        connectUsers.document(userNickname)
//                .collection("person_data").document("storyTest").set(mapOf(
//                    "shop" to titleItem,
//                    "money" to money
//                ))
//    }

    fun initUser(uid: String?): Task<Void> {
        return connectUsers.document(uid!!).set(User(uid))
    }

    fun getUser(uid: String?): Task<DocumentSnapshot> {
        return connectUsers.document(uid!!).get()
    }

    fun editNicknameUser(uid: String?, newNickname: String): Task<Void> {
        return connectUsers.document(uid!!).update(mapOf("nickname" to newNickname))
    }

    fun editDescriptionUser(uid: String?, newDescription: String): Task<Void> {
        return connectUsers.document(uid!!).update(mapOf("description" to newDescription))
    }
}