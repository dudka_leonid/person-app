package e.personapp.study_ino.database

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import e.personapp.castom_test.model.Test
import e.personapp.study_ino.model.EnglishText

class DataBaseINO {

    val texts = ArrayList<EnglishText>()

    private var myDB: FirebaseFirestore = FirebaseFirestore.getInstance()
    private var connect: CollectionReference = myDB.collection("englishTexts")

    fun loadEnglishText(title: String, englishTexts: ArrayList<EnglishText>)
            : Task<DocumentSnapshot> {
        return connect.document(title).get().addOnSuccessListener {
            it.data?.forEach{ _, text ->
                englishTexts.add(castText(text as HashMap<String, Any>))
            }
        }
    }

    private fun castText(hashMap: HashMap<String, Any>): EnglishText {
        return EnglishText(
            hashMap["titleEn"].toString(), hashMap["en"].toString(),
            hashMap["titleRu"].toString(), hashMap["ru"].toString())
    }

    fun loadEnglishTextTitles(titles: ArrayList<String>): Task<QuerySnapshot> {
        return connect.get().addOnSuccessListener {
            it.forEach { q ->
                titles.add(q.id)
            }
        }
    }

    fun storyEnglishTexts(title: String): Task<Void> {
        val map = HashMap<String, Any>()
        var i = 1
        texts.forEach {
            map[i++.toString()] = it
        }
        return connect
            .document(title)
            .set(map)
    }
}