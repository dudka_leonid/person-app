package e.personapp.study_ino.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import e.personapp.R
import e.personapp.study_ino.model.EnglishText

class EnglishTextAdapter(
    private var texts: ArrayList<EnglishText>
): RecyclerView.Adapter<EnglishTextAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutView = LayoutInflater.from(parent.context)
            .inflate(R.layout.english_text_item, parent, false)
        return ViewHolder(layoutView)
    }

    override fun getItemCount(): Int {
        return texts.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(texts[position])
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val titleEn: TextView = itemView.findViewById(R.id.title_text_en_TextView)
        private val textEn: TextView = itemView.findViewById(R.id.text_en_TextView)
        private val titleRu: TextView = itemView.findViewById(R.id.title_text_ru_TextView)
        private val textRu: TextView = itemView.findViewById(R.id.text_ru_TextView)

        fun bind(englishText: EnglishText) {
            titleEn.text = englishText.titleEn
            textEn.text = parse(englishText.en)
            titleRu.text = englishText.titleRu
            textRu.text = parse(englishText.ru)
        }

        fun parse(text: String): String {
            var result = ""
            text.split('#').forEach {
                result += "\t $it \n\n"
            }
            return result
        }
    }
}