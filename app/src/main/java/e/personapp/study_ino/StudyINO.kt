package e.personapp.study_ino

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import e.personapp.R
import e.personapp.study_ino.adapters.EnglishTextAdapter
import e.personapp.study_ino.database.DataBaseINO
import e.personapp.study_ino.model.EnglishText
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class StudyINO : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_study_ino)

        val data = DataBaseINO()

        val recyclerView: RecyclerView = findViewById(R.id.english_text_RecyclerView)
        var englishTextAdapter: EnglishTextAdapter

        val titles = ArrayList<String>()
        data.loadEnglishTextTitles(titles)
            .addOnCompleteListener {
                Log.e("test_StudyINO", titles.toString())

                val stage = ArrayList<EnglishText>()
                data.loadEnglishText(titles[0], stage)
                    .addOnCompleteListener {
                        englishTextAdapter = EnglishTextAdapter(stage)
                        recyclerView.adapter = englishTextAdapter
                    }
            }


    }
}
