package e.personapp.study_ino.model

data class EnglishText(var titleEn: String, var en: String, var titleRu: String, var ru: String)