package e.personapp.utils

import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MyTimer {
    // Calendar.getInstance().time - получить дату с телефона
    fun timerUp(seconds: Long, period: Long = 1000) {
        GlobalScope.launch {
            for (i: Long in 0..seconds) {
                delay(period)
                Log.e("test_IEditTest", "timer: $i")
            }
        }
    }

    fun timerDown(seconds: Long, period: Long = 1000) {
        GlobalScope.launch {
            for (i: Long in seconds downTo 0) {
                delay(period)
                Log.e("test_IEditTest", "timer: $i")
            }
        }
    }
}