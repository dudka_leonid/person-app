package e.personapp.castom_test.database

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import e.personapp.castom_test.model.Test
import e.personapp.castom_test.model.TestMaker


/**
 * Примиер работы этого класса
 *
 *           val dataBaseTest = DataBaseTest()
 *           dataBaseTest.testMaker.addTest(Test("вопрос 1", "ответ 1"))
 *           dataBaseTest.testMaker.addTest(Test("вопрос 2", "ответ 2"))
 *           dataBaseTest.testMaker.addTest(Test("вопрос 3", "ответ 3"))
 *           dataBaseTest.storyTest("test2")
 *
 *           dataBaseTest.loadTest("test2").get().addOnSuccessListener {
 *              Log.e("test_tests_working", it.data.toString())
 *          }
 *          minusSize_button.setOnClickListener {
 *              dataBaseTest.deleteTest("test2")
 *          }
 *
 **/
class DataBaseTest {

    val testMaker: TestMaker = TestMaker()

    private var myDB: FirebaseFirestore = FirebaseFirestore.getInstance()
    private var connectTests: CollectionReference = myDB.collection("tests")

    fun loadTest(
        testName: String,
        dictionary: ArrayList<Test>
    ): Task<DocumentSnapshot> {
        return connectTests.document(testName).get().addOnSuccessListener{
            it.data?.forEach { _, t ->
                dictionary.add(testMaker.castTest(t as HashMap<String, String>))
            }
        }
    }

    fun storyTest(testName: String): Task<Void> {
        val map = HashMap<String, Any>()
        var i = 1
        testMaker.tests.forEach {
            map[i++.toString()] = it
        }
        return connectTests
            .document(testName)
            .set(map)
    }

    fun deleteTest(testName: String): Task<Void> {
        return connectTests.document(testName).delete()
    }

    fun loadTestTitles(titles: ArrayList<String>): Task<QuerySnapshot> {
        return connectTests.get().addOnSuccessListener {
            it.forEach { q ->
                titles.add(q.id)
            }
        }
    }
}