package e.personapp.castom_test.interfaces

interface ICreateNewTest {
    fun createTest()
    fun finishCreateTest(tagFragment: String)
}