package e.personapp.castom_test.interfaces

interface IEditTestsRecycler {
    fun openTest(title: String)
    fun editTest(title: String)
    fun deleteTest(title: String)
}