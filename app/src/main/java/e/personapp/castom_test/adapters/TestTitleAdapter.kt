package e.personapp.castom_test.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import e.personapp.R
import e.personapp.castom_test.interfaces.IEditTestsRecycler

class TestTitleAdapter(
    private val titles: List<String>,
    private val context: Context
):
    RecyclerView.Adapter<TestTitleAdapter.ViewHolderWord>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderWord {
        val layoutView = LayoutInflater.from(parent.context)
            .inflate(R.layout.test_title_item, parent, false)
        return ViewHolderWord(layoutView)
    }

    override fun getItemCount(): Int {
        return titles.size
    }

    override fun onBindViewHolder(holder: ViewHolderWord, position: Int) {
        holder.bind(titles[position], context)
    }

    class ViewHolderWord(itemView: View):
        RecyclerView.ViewHolder(itemView){

        private lateinit var popupMenu: PopupMenu
        var title: TextView = itemView.findViewById(R.id.title_test_test_title_item_TextView)
            private set

        fun bind(title: String, context: Context) {
            this.title.text = title
            val mCallback = context as IEditTestsRecycler
            itemView.setOnClickListener {
                popupMenu = PopupMenu(context, it)
                popupMenu.inflate(R.menu.tests_item_menu)
                popupMenu.setOnMenuItemClickListener { v ->
                    return@setOnMenuItemClickListener when (v.itemId) {
                        R.id.openTest -> {
                            mCallback.openTest(title)
                            true
                        }
                        R.id.editTest -> {
                            mCallback.editTest(title)
                            true
                        }
                        R.id.deleteTest -> {
                            mCallback.deleteTest(title)
                            true
                        }
                        else -> false
                    }
                }
                popupMenu.show()
            }
        }
    }
}