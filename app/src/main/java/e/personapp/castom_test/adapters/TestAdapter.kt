package e.personapp.castom_test.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import e.personapp.R
import e.personapp.castom_test.model.Test


class TestAdapter(
    private val tests: List<Test>
):
    RecyclerView.Adapter<TestAdapter.ViewHolderWord>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderWord {
        val layoutView = LayoutInflater.from(parent.context)
            .inflate(R.layout.test_item, parent, false)
        return ViewHolderWord(layoutView)
    }

    override fun getItemCount(): Int {
        return tests.size
    }

    override fun onBindViewHolder(holder: ViewHolderWord, position: Int) {
        holder.bind(tests[position])
    }

    class ViewHolderWord(itemView: View): RecyclerView.ViewHolder(itemView){
        var question: TextView = itemView.findViewById(R.id.question_TextView)
            private set
        var answer: TextView = itemView.findViewById(R.id.answer_TextView)
            private set

        fun bind(test: Test) {
            question.text = test.question
            answer.text = test.answer
        }
    }
}