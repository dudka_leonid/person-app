package e.personapp.castom_test.model

class TestMaker {

    var tests = ArrayList<Test>()
        private set

    fun addTest(test: Test) {
        tests.add(test)
    }

    fun castTest(map: HashMap<String, String>): Test {
        return Test(map["question"]!!, map["answer"]!!)
    }
}