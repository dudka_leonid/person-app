package e.personapp.castom_test.model

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import e.personapp.R
import e.personapp.castom_test.database.DataBaseTest
import e.personapp.castom_test.fragments.TestAddNewFragment
import e.personapp.castom_test.fragments.TestManagerFragment
import e.personapp.castom_test.fragments.TestsListFragment
import e.personapp.castom_test.interfaces.ICreateNewTest
import e.personapp.castom_test.interfaces.IEditTestsRecycler


class Tests : AppCompatActivity(), IEditTestsRecycler, ICreateNewTest {

    private lateinit var testsListFragment: TestsListFragment
    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var testManagerFragment: TestManagerFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val connect = intent.getBooleanExtra("connect", false)
        if (connect) {
            setContentView(R.layout.activity_tests)
            initFragments()
        } else {
            setContentView(R.layout.stub_error_layout)
        }
    }

    // Инициализируем систему смены фрагментов
    private fun initFragments() {
        testsListFragment = TestsListFragment()
        fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frgmCont, testsListFragment, testsListFragment.title)
        fragmentTransaction.commit()
    }

    override fun createTest() {
        val testAddNewFragment = TestAddNewFragment()
        fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frgmCont, testAddNewFragment,
            testAddNewFragment.title)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    override fun finishCreateTest(tagFragment: String) {
        onBackPressed()
        fragmentTransaction = supportFragmentManager.beginTransaction()
        supportFragmentManager.findFragmentByTag(tagFragment)?.let {
            fragmentTransaction.remove(it)
        }
        fragmentTransaction.commit()
    }

    override fun openTest(title: String) {
        val testManagerFragment = TestManagerFragment(title)
        fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frgmCont, testManagerFragment,
            testManagerFragment.title)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
        Log.e("test_IEditTest", "openTest $title")
    }

//ToDo реализуй эти методы!!!-----------------------------------------------------------------------
    override fun editTest(title: String) {
        val data = DataBaseTest()
        data.loadTest(title, data.testMaker.tests)
            .addOnCompleteListener {
                val testAddNewFragment = TestAddNewFragment(title, data.testMaker)
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.frgmCont, testAddNewFragment,
                    testAddNewFragment.title)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
        }
    }

    override fun deleteTest(title: String) {
        val testsListFragment: TestsListFragment = supportFragmentManager
            .findFragmentByTag(TestsListFragment().title) as TestsListFragment
        testsListFragment.deleteTest(title)
    }
}
