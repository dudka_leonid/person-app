package e.personapp.castom_test.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import e.personapp.R
import e.personapp.castom_test.adapters.TestTitleAdapter
import e.personapp.castom_test.database.DataBaseTest
import e.personapp.castom_test.interfaces.ICreateNewTest

class TestsListFragment: Fragment(){

    val title: String = "TestsListFragment"
    private lateinit var recyclerView: RecyclerView
    private lateinit var testTitleAdapter: TestTitleAdapter
    private lateinit var titles: ArrayList<String>
    private lateinit var testInfoTextView: TextView
    private lateinit var addTestButton: FloatingActionButton
    private lateinit var mCallback: ICreateNewTest
    private lateinit var data: DataBaseTest

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.tests_list_fragment,
            container, false)

        data = DataBaseTest()
        recyclerView = view.findViewById(R.id.tests_list_fragment_RecyclerView)
        titles = ArrayList()

        data.loadTestTitles(titles)
            .addOnCompleteListener {
                testTitleAdapter = TestTitleAdapter(titles, view.context)
                recyclerView.adapter = testTitleAdapter
            }

        addTestButton = view.findViewById(R.id.add_new_test_floatingActionButton)
        testInfoTextView = view.findViewById(R.id.test_info_textView)
        mCallback = activity as ICreateNewTest

        //переход к созданию нового теста
        addTestButton.setOnClickListener {
            mCallback.createTest()
        }

        return view
    }

    fun deleteTest(title: String) {
        data.deleteTest(title)
            .addOnCompleteListener {
                testTitleAdapter.notifyItemRemoved(titles.indexOf(title))
                titles.remove(title)
        }
    }
}