package e.personapp.castom_test.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TableLayout
import android.widget.TableRow
import androidx.core.view.children
import androidx.fragment.app.Fragment
import e.personapp.R
import e.personapp.castom_test.database.DataBaseTest
import e.personapp.castom_test.interfaces.ICreateNewTest
import e.personapp.castom_test.model.Test
import e.personapp.castom_test.model.TestMaker

class TestAddNewFragment(): Fragment() {

    val title: String = "TestAddNewFragment"
    lateinit var storyButton: Button
    private lateinit var mCallback: ICreateNewTest
    private lateinit var tableLayout: TableLayout
    private lateinit var testNameEditText: EditText

    private var testMaker: TestMaker? = null
    private lateinit var titleTest: String

    constructor(titleTest: String, testMaker: TestMaker): this() {
        this.testMaker = testMaker
        this.titleTest = titleTest
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.test_add_new_fragment,
                                            container, false)
        storyButton = view.findViewById(R.id.add_new_test_button)
        val data = DataBaseTest()
        mCallback = activity as ICreateNewTest

        tableLayout = view.findViewById(R.id.tests_test_add_new_fragment_TableLayout)
        testNameEditText = view.findViewById(R.id.test_name_test_add_new_fragment_EditText)

        if (this.testMaker != null) initTestMaker()

        //Возвращает обратно к менаджер окну
        storyButton.setOnClickListener {
            val testName = testNameEditText.text?.toString()
            var iterator: Iterator<View>
            var question: String
            var answer: String
            tableLayout.children.forEach {
                // каждый TableRow
                iterator = (it as TableRow).children.iterator()
                question = (iterator.next() as EditText).text.toString()
                answer = (iterator.next() as EditText).text.toString()

                if (question.isNotEmpty() && answer.isNotEmpty()) {
                    data.testMaker.addTest(Test(question, answer))
                }
            }
            if (data.testMaker.tests.size > 0 && !(testName.isNullOrEmpty())) {
                if (this.titleTest.isNotEmpty()) {
                    data.deleteTest(this.titleTest)
                }
                data.storyTest(testName)
            }
            mCallback.finishCreateTest(title)
        }
        return view
    }

    // Log.e("test_TestAddNewFragment", "")

    private fun initTestMaker() {
        testNameEditText.setText(this.titleTest)
        var iterator: Iterator<View>
        val tableLayoutIterator = tableLayout.children.iterator()
        this.testMaker!!.tests.forEach {
            iterator = (tableLayoutIterator.next() as TableRow).children.iterator()
            (iterator.next() as EditText).setText(it.question)
            (iterator.next() as EditText).setText(it.answer)
        }
    }
}