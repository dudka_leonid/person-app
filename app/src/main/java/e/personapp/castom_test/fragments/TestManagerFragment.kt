package e.personapp.castom_test.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import e.personapp.R
import e.personapp.castom_test.adapters.TestAdapter
import e.personapp.castom_test.database.DataBaseTest
import e.personapp.castom_test.model.Test

class TestManagerFragment(private val testName: String): Fragment() {

    val title: String = "TestManagerFragment"

    private lateinit var recyclerView: RecyclerView
    private lateinit var testAdapter: TestAdapter
    private lateinit var dictionary: ArrayList<Test>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(
            R.layout.test_manager_fragment,
            container, false)

        recyclerView = view.findViewById(R.id.test_list_recyclerView)


//--------(Запрос к БД)-----------------------------------------------------------------------------
        val data = DataBaseTest()
        dictionary = ArrayList()
        data.loadTest(testName, dictionary)
            .addOnCompleteListener{
                // мне понравилась эта сортировка
                dictionary.sortedWith(compareBy(Test::question))
                testAdapter = TestAdapter(dictionary)
                recyclerView.adapter = testAdapter
            }
//--------------------------------------------------------------------------------------------------
//
//        //ToDo подумай как сделать лучше всего:
//        // 1) Отображение списка тестов, да это важно;
//        // 2) Создай отдельный фрагмент (класс) и перенеси туда всю логику;
//        // 3) проверь работу всего;
//        // 4) настрой систему создания\редактирования\удаления тестов

        return view
    }
}