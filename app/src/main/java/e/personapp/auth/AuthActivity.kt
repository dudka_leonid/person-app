package e.personapp.auth

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import e.personapp.R
import e.personapp.main.MainActivity
import e.personapp.user.DataBaseUser

class AuthActivity : AppCompatActivity() {

    private lateinit var emailEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var singUpButton: Button
    private lateinit var singInButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.auth_activity)

        emailEditText = findViewById(R.id.email_edit_text_auth_activity)
        passwordEditText = findViewById(R.id.password_edit_text_auth_activity)
        singUpButton = findViewById(R.id.sing_up_button_auth_activity)
        singInButton = findViewById(R.id.sing_in_button_auth_activity)

        val authService = AuthService()
        val uid =  authService.getUID()
        if (!uid.isNullOrEmpty()) {
            makeToast(getString(R.string.authorization_successful))!!.show()
            runActivity()
        }

        singUpButton.setOnClickListener {
            val result = authService
                .registration(emailEditText.text.toString(), passwordEditText.text.toString())
            if (result != null) {
                result.addOnCompleteListener {
                    if (it.isSuccessful) {
                        val dataBaseUser = DataBaseUser()
                        dataBaseUser.initUser(authService.getUID())
                        makeToast(getString(R.string.registration_successful))!!.show()
                        runActivity()
                    } else {
                        makeToast(getString(R.string.registration_error))!!.show()
                    }
                }
            } else {
                makeToast(getString(R.string.empty_input_fields))!!.show()
            }
        }
        singInButton.setOnClickListener {
            val result = authService
                .authorization(emailEditText.text.toString(), passwordEditText.text.toString())
            if (result != null) {
                result.addOnCompleteListener {
                    if (it.isSuccessful) {
                        makeToast(getString(R.string.authorization_successful))!!.show()
                        runActivity()
                    } else {
                        makeToast(getString(R.string.authorization_error))!!.show()
                    }
                }
            } else {
                makeToast(getString(R.string.empty_input_fields))!!.show()
            }
        }
    }

    private fun runActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun makeToast(text: String): Toast? {
        return Toast.makeText(this, text, Toast.LENGTH_SHORT)
    }
}