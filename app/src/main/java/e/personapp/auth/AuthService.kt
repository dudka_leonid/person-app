package e.personapp.auth

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth

class AuthService {

    private var mAuth: FirebaseAuth = FirebaseAuth.getInstance()

    fun getUID(): String? {
        return mAuth.currentUser?.uid
    }

    fun signOut() {
        mAuth.signOut()
    }

    fun registration(email: String, password: String): Task<AuthResult>? {
        return if (email.isNotEmpty() && password.isNotEmpty()) {
            mAuth.createUserWithEmailAndPassword(email, password)
        } else {
            null
        }
    }

    fun authorization(email: String, password: String): Task<AuthResult>? {
        return if (email.isNotEmpty() && password.isNotEmpty()) {
            mAuth.signInWithEmailAndPassword(email, password)
        } else {
            null
        }
    }
}